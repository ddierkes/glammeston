import termcolor
from glammeston.record import Record


class NPC(Record):
    """
        Keeps track of the fields and questions needed to populate those fields
        for an NPC record to be generated, retrieved, and used.
    """
    name = ""
    image = ""
    race = ""
    occupation = ""
    place = []
    affiliation = []
    bio = ""
    fields = [
        "id", "name", "image", "race", "occupation", "place", "affiliation", "bio"
    ]
    questions = {
        "intro": "Ah, so you want to make a non-player character record, eh?  Go ahead.",
        "name": {"list": False, "question": "Who is this? "},
        "image": {"list": False, "question": "What is the name of the image? "},
        "race": {"list": False, "question": "What race? "},
        "occupation": {"list": False, "question": "What does this person do? "},
        "place": {"list": True, "question": "Is this person associated with a place? "},
        "affiliation": {
            "list": True, "question": "Is this person associated with a group? "
        },
        "bio": {"list": False, "question": "Describe this person in about 200 words "},
    }


class Place(Record):
    id = ""
    name = ""
    image = ""
    map = ""
    x_coordinate = ""
    y_coordiante = ""
    description = ""
    shops = []
    type = []
    fields = [
        "id",
        "name",
        "image",
        "map",
        "x_coordinate",
        "y_coordinate",
        "description",
        "shops",
        "type",
    ]
    questions = {
        "intro": "Ah, so you want to make a place record, eh?  Go ahead.",
        "name": {"list": False, "question": "Who is this? "},
        "image": {
            "list": False,
            "question": "What is the name of the image representing this place? ",
        },
        "map": {"list": False, "question": "What map does this place appear on? "},
        "x_coordinate": {
            "list": False,
            "question": "What is the x coordinate for this place on the map? ",
        },
        "y_coordinate": {
            "list": False,
            "question": "What is the y coordinate for this place on the map? ",
        },
        "description": {
            "list": False, "question": "Describe this place in a general sense? "
        },
        "shops": {"list": True, "question": "Are there any shops in this place? "},
        "type": {
            "list": True,
            "question": "Is there a descriptive term for this type of place? ",
        },
    }


class Vehicle(Record):
    """Encounters require a broader structure to be built, so will return to this."""
    id = ""
    name = ""
    image = ""
    description = ""


class Shop(Record):
    """Encounters require a broader structure to be built, so will return to this."""
    id = ""
    name = ""
    image = ""
    description = ""


class Encounter(Record):
    """Encounters require a broader structure to be built, so will return to this."""
    id = ""
    name = ""
    condition_of_occurance = {}
    fields = ["name", "condition_of_occurance"]

    def cli_generate(self, state):
        self.name = input(
            termcolor.colored("What is the name of this encounter:  ", "green")
        )
        for key, value in state.items():
            print(
                "Does the global state variable "
                + key
                + " have any bearing on this encounter?"
            )
            while True:
                a = input(
                    termcolor.colored(
                        "Does the global state variable "
                        + key
                        + " have any bearing on this encounter? y o n: ",
                        "green",
                    )
                )
                if a == "n":
                    self.condition_of_occurance[key] = "not important"
                    break
                elif a == "y":
                    if value["type"] == "number":
                        while True:
                            condition = []
                            b = input(
                                termcolor.colored(
                                    "At what number is the threshold for " + key,
                                    "green",
                                )
                            )
                            try:
                                condition.append(int(b))
                            except:
                                print("please provide a number")
                                continue
                            while True:
                                c = input(
                                    termcolor.colored(
                                        "Does "
                                        + key
                                        + " need to be (1) more than "
                                        + b
                                        + " or (2) less than "
                                        + b,
                                        "green",
                                    )
                                )
                                if c == "1":
                                    condition.append("more than")
                                    break
                                elif c == "2":
                                    condition.append("less than")
                                    break
                                else:
                                    print("please choose 1 or 2")
                            self.condition_of_occurance[key] = condition
                            break

                    elif value["type"] == "string":
                        while True:
                            condition = []
                            b = input(
                                termcolor.colored(
                                    "Are there any particular values of "
                                    + key
                                    + " in which this encounter occurs? y or no",
                                    "green",
                                )
                            )
                            if b == "y":
                                while True:
                                    c = input(
                                        termcolor.colored(
                                            "What is the value? ", "green"
                                        )
                                    )
                                    d = input(
                                        termcolor.colored(
                                            "Add " + c + " as a condition? y or n. ",
                                            "green",
                                        )
                                    )
                                    if d == "y":
                                        condition.append(c)
                                        break
                                    else:
                                        print("Discarding " + c)
                            elif b == "no":
                                self.condition_of_occurance[key] = condition
                                break
                    elif value["type"] == "bool":
                        while True:
                            condition = []
                            b = input(
                                termcolor.colored(
                                    "Must " + key + "be (1) true or (2) false", "green"
                                )
                            )
                            if b == "1":
                                self.condition_of_occurance[key] = "must be true"
                                break
                            elif b == "2":
                                self.condition_of_occurance[key] = "must be false"
                                break
                            else:
                                print("Please choose 1 or 2")
