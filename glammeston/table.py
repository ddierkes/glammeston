from glob import glob

import pandas as pd
import termcolor


class Table(object):
    """
    This is not a relational table.  It is just piled up data for lookups.
    It is really just a convenient way of accessing csvs and dealing with them as pandas DataFrames.
    """
    name = ""

    def __init__(self, df, name, data_path):
        """
            take a pandas dataframe and instantiate the class with it.
        """
        self.df = df
        self.name = name
        self.date_path = data_path

    def save(self):
        """overwrites a csv file to store the data"""
        self.df.to_csv(self.date_path + '/' + self.name + '.csv')
