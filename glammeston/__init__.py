"""glammeston - a reusable core for generating object data for text and tabletop games"""

__version__ = "0.1.0"
__author__ = "Daron Dierkes <daron.dierkes@gmail.com>"
__all__ = []


import glob, json
import pandas as pd


def get_record(key, value, data_path):
    """Returns all records that share a certain value in a certain field"""
    data = []
    for file_name in glob.glob(data_path + "/*.json"):
        with open(file_name) as f:
            character = json.load(f)
            if value in character[key]:
                data.append(character)
    return data



def get_all(data_path, page=1, size=8):
    """Returns all the NPC records that fit on a given page"""
    if page < 1:
        print("pages cannot be negative")
        return []
    page = int(page)
    size = int(size)
    data = []
    for index, file_name in enumerate(glob.glob(data_path + "/*.json")):
        npc_number = index + 1
        if npc_number <= (page * size) and npc_number > size * (page - 1):
            with open(file_name) as f:
                character = json.load(f)
                data.append(character)
        else:
            print("npc_number is not good")
    return data


def load_record(name, recordClass, data_path):
    """Takes a name string, a class to instantiate, and where to get the record"""
    data = get_record("name", name, data_path)
    if len(data) == 1:
        data = data[0]
        this_guy = recordClass(data, data_path)
    else:
        this_guy = "TROUBLE, There is more than one instance of " + name
        print(this_guy)
    return this_guy

def load_table(table_name, tableClass, data_path):
    """with a name and path to a csv, return it as a DataFrame"""
    try:
        df = pd.read_csv(data_path + '/' + table_name + '.csv')
    except:
        return False, ("There is no data named " + table_name)
    this_data = tableClass(df, table_name, data_path)
    return this_data
