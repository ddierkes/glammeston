import uuid, json


import termcolor


class Record(object):
    name = ""
    fields = []
    questions = {
        "intro": "Ah, so you want to make a person record, eh?  Go ahead.",
        "name": {"list": False, "question": "Who is this? "},
    }

    def __init__(self, data={}, data_path):
        """
            if the class is instantiated from a previous record it is important that the id
            travel with it.  Otherwise, a new id needs to be made.
        """
        self.data_path = data_path
        if "id" in data:
            self.load_from_dict(data)
        else:
            self.id = str(uuid.uuid4())

    def load_from_dict(self, data):
        """
        given an full or partial dictionary, this will populate it.
        """
        for field in self.fields:
            if field in data:
                setattr(self, field, data[field])

    def get_dict(self):
        """
        Calling the .get_dict() method will provide the wrapper code with a dictionary
        with all the relevant data about the record.   This is important because data stored
        as a dictionary can be reinstantiated through this method.
        """
        obj = {}
        for field in self.fields:
            obj[field] = getattr(self, field)
        return obj

    def cli_generate(self):
        print(termcolor.colored(self.questions["intro"], "cyan"))
        for field in self.fields:
            if field != "id":
                if self.questions[field]["list"]:
                    answers = []
                    while True:
                        a = input(
                            termcolor.colored(
                                self.questions[field]["question"], "green"
                            )
                        )
                        if a == "no" or a == "stop" or a == "n":
                            break
                        else:
                            answers.append(a)
                        b = input(
                            termcolor.colored("Add another (y or n)?  ", "yellow")
                        )
                        if b == "no" or b == "stop" or b == "n":
                            break
                    setattr(self, field, answers)
                else:
                    setattr(
                        self,
                        field,
                        input(
                            termcolor.colored(
                                self.questions[field]["question"], "green"
                            )
                        ),
                    )
        print("record updated.  It appears as follows")
        print(self.get_dict())

    def print_stats(self):
        for field in self.fields:
            print(termcolor.colored(field + ":  ", "green") + str(getattr(self, field)))

    def save(self):
        """Coverts the NPC's data to a json file"""
        s = json.dumps(self.get_dict())
        with open((self.data_path + "/" + self.id + ".json"), "w") as f:
            f.write(s)
