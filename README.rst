glammeston
==========

.. image:: https://img.shields.io/pypi/v/glammeston.svg
    :target: https://pypi.python.org/pypi/glammeston
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal.png
   :target: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal
   :alt: Latest Travis CI build status

a reusable core for generating object data for text and tabletop games

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`glammeston` was written by `Daron Dierkes <daron.dierkes@gmail.com>`_.
